-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 10, 2019 at 01:21 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `artist_booking`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
CREATE TABLE IF NOT EXISTS `booking` (
  `bookingid` int(255) NOT NULL AUTO_INCREMENT,
  `artisttype` varchar(255) DEFAULT NULL,
  `artistname` varchar(255) DEFAULT NULL,
  `contactperson` varchar(255) DEFAULT NULL,
  `contactnumber` varchar(255) DEFAULT NULL,
  `contactmail` varchar(255) DEFAULT NULL,
  `contactadd` varchar(255) DEFAULT NULL,
  `contactcity` varchar(255) DEFAULT NULL,
  `pincode` varchar(255) DEFAULT NULL,
  `numbersofbooking` varchar(255) DEFAULT NULL,
  `bookingdate` date DEFAULT NULL,
  `reportingtime` time(6) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `eventtype` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`bookingid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`bookingid`, `artisttype`, `artistname`, `contactperson`, `contactnumber`, `contactmail`, `contactadd`, `contactcity`, `pincode`, `numbersofbooking`, `bookingdate`, `reportingtime`, `duration`, `eventtype`) VALUES
(1, 'Farukh', 'CELEBRITY ', 'qumber', '123235', 'xyz@mail.com', 'nhi batana ', 'delhi', '110062', '', '2019-02-12', NULL, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
